package com.doha.backofficeservicemb.service;

import java.util.Map;

import com.doha.backofficeservicemb.model.GenericResponse;

public interface I18Service {

	GenericResponse<Map<String, String>> getLabelByLang(String unit, String channel, String lang, String screenId);

}
