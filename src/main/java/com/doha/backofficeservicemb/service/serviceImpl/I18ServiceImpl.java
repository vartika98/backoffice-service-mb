package com.doha.backofficeservicemb.service.serviceImpl;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.doha.backofficeservicemb.constant.AppConstant;
import com.doha.backofficeservicemb.entity.I18N;
import com.doha.backofficeservicemb.model.GenericResponse;
import com.doha.backofficeservicemb.model.ResultUtilVO;
import com.doha.backofficeservicemb.repository.I18NRepository;
import com.doha.backofficeservicemb.service.I18Service;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class I18ServiceImpl implements I18Service {

	private ResultUtilVO resultVo = new ResultUtilVO();

	@Autowired
	private I18NRepository i18NRepo;

	@Override
	public GenericResponse<Map<String, String>> getLabelByLang(String unit, String channel, String lang,
			String screenId) {
		var response = new GenericResponse<Map<String, String>>();
		var resMap = new HashMap<String, String>();
		try {
			var labelLst = i18NRepo.findByUnit_UnitIdAndChannel_ChannelIdAndLangCodeAndPage(unit, channel, lang,
					screenId);
			resMap = (HashMap<String, String>) labelLst.stream()
					.collect(Collectors.toMap(I18N::getKey, I18N::getValue));
			response.setData(resMap);
			resultVo = new ResultUtilVO(AppConstant.RESULT_CODE, AppConstant.RESULT_DESC);
		} catch (Exception e) {
			log.info("Exception in getLabelByLang :{}", e);
			resultVo = new ResultUtilVO(AppConstant.GEN_ERROR_CODE, AppConstant.GEN_ERROR_DESC);
		}
		response.setStatus(resultVo);
		return response;
	}

}
