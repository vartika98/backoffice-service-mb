package com.doha.backofficeservicemb.constant;

public interface AppConstant {

	 

    // error response

    public static final String GEN_ERROR_CODE = "GENER_CODE";

    public static final String GEN_ERROR_DESC = "Unable to process your request,Please contact Customer Care for futher assistance or try again later";

    public static final String GEN_ERROR_DESC_EN = "Unable to process your request,Please contact Customer Care for futher assistance or try again later";

    public static final String GEN_ERROR_DESC_AR = "Unable to process your request,Please contact Customer Care for futher assistance or try again later";

    public static final String GEN_ERROR_DESC_FR = "Unable to process your request,Please contact Customer Care for futher assistance or try again later";

    public static final String METADATA_GROUP  = "Default";
    public static final String YYYYMMDD = "yyyy-MM-dd HH:mm:ss";

    public static final String DDMMYYYY = "dd/MM/yyyy HH:mm:ss";

    public static final String USERNO = "userNo";
    public static final String GID = "gId";
    public static final String EN = "en";

    public static final String AR = "ar";

    public static final String FR = "fr";

 

    public static final String DEFAULT_STATUS = "A";

    public static final String DEFAULT_STATUS_ACT = "ACT";

    public static final String DEFAULT_STATUS_PND = "PND";

    public static final String RESULT_CODE = "000000";

    public static final String RESULT_DESC = "SUCCESS";

    public static final String DEL = "DEL";

 

    // header constant

    public static final String HEADER_ACCEPT_LANGUAGE = "Accept-Language";

    public static final String HEADER_UNIT = "unit";

    public static final String HEADER_CHANNEL = "channel";

    public static final String HEADER_GID = "gId";

    public static final String HEADER_USERNO = "userNo";

    public static final String HEADER_USER_MOBILE_NO = "mobileNumber";

    public static final String HEADER_COUNTRY_CODE = "countryCode";

    public static final String HEADER_USER_NAME = "username";

    public static final String HEADER_PREF_UNITS = "preferredUnits";

    public static final String HEADER_ACC_NO = "accNo";


    public static final String DEFAULT_UNIT = "PRD";


    public static final String OCS_DATE_FORMAT = "dd/MM/yyyy";

    public static final String OCS_DATE_FORMAT_2= "dd-MM-yyyy";


    public static final String CODE = "code";

    public static final String DESCRIPTION = "description";

    public static final String USERAGENT = "User-Agent";

    public static final String FAILURE = "Failure";

    public static final String SUCCESS = "Success";

    public static final String REQUEST_ENTITY = "entity";

    public static final String RESPONSE_BODY = "body";

    public static final String RESPONSE_RESULT = "result";

    public static final String RESPONSE_DATA = "data";

    public static final String EXCEPTION_MSG = "EXCEPTION_MSG";

    public static final String AUDIT_REFNO = "auditRefNo";

    public static final String SERVICEID = "serviceId";


    public static final String SERVIC_TYPE = "MW";

    public static final String PUBLIC_URL = "/public";

    public static final String MW_DATE_FORMAT = "yyyy-MM-dd";

    public static final String USERNAME = "userName";

    public static final String MOBILE_NO = "mobileNo";

    public static final String MOBILE_NUMBER = "mobileNumber";

    public static final String EMAIL = "email";

    public static final String FIRST_NAME = "firstName";

    public static final String LAST_NAME = "lastName";

    public static final String FULL_NAME = "fullName";

    public static final String MIDDLE_NAME = "middleName";

    public static final String USER_EMAIL = "EmailId";

    public static final String NA = "NA";

    public static final String COUNTRY_CODE = "countryCode";

    public static final String SESSION_ID = "sessionId";

    public static final String BROWSER = "browser";

    public static final String IP_ADDRESS = "ipaddr";

    public static final String YES = "Y";

    public static final String NO = "N";

    public static final String ALL = "ALL";

    public static final String TRUE = "true";

    public static final String FALSE = "false";

    public static final String BRANCH_NAME = "branchName";

    public static final String NAME = "name";

    public static final String BRANCH_CODE = "branchCode";

    public static final String BANKCURCODE = "currencyCode";

    public static final String FUNCTIONAL_ID = "functionalId";

    public static final String ACNO = "acNo";

    public static final String GUID = "guid";

    public static final String CUSTOMER_SEGMENT = "customerSegment";

    public static final String CUST_TYPE = "custType";

    public static final String N = "N";

    public static final String Y = "Y";

    public static final Object STATUS = "STATUS";

    public static final Object DATE = "DATE";

    public static final String UNIT_ID = "unitId";
    public static final String CHANNEL_ID = "channelId";

    public static final String mwDateFormat = "yyyy-MM-dd";

    public static final String NID_SOURCE_DATE_FORMAT = "dd-MMM-yyyy";

    public static final String NID_DEST_DATE_FORMAT = "yyyy-MM-dd";

    public static final String DEFAULT_CREATED_BY = "SYSTEM";

    public static final Object HEADER_NOTE = "HEADER_NOTE";

    public static final Object FOOTER_NOTE = "FOOTER_NOTE";

    public static final Object FOOTER_NOTE2 = "FOOTER_NOTE2";

    public static final int PHONE_LENGTH = 11;

    public static final String MOBILE_PREFIX = "974";

    public static final String STRING_SPACE_EMPTY = " ";

    public static final String NATIONAL_ID = "nationalId";

    public static final String RESULT_ERROR_CODE = "000001";

    public static final String RESULT_ERROR_DESC = "FAILED";

    public static final String MB_CHANNEL_HEADER = "MB";

    public static final String ENRY_TRUE = "TRUE";

    public static final String DEFAULT_BRANCHCODE = "0011";

    public static final String BASE_NO = "baseNo";

    public static final String INQUIRY_TYPE = "inquiryType";;

    public static final String AUDIT_UNIT = "UNIT";

    public static final String AUDIT_CHANNEL = "CHANNEL";

    public static final String CUST_EMAILID = "emailId";
    public static final String  MODULEID = "moduleId";

    public static final String  SUBMODULEID = "subModuleId";

    public static final String DEVICE_TYPE = "device_type";

    public static final String DEVICE_MODEL = "device_model";

    public static final String OS_VERSION = "os_version";

    public static final String IMEI_NO = "imei";

    public static final String GEO_LOCATION = "geo_location";


    public static final String TXNREFID = "txnRefId";


    public static final String SUC = "SUC";    

    public static final String PND = "PND";    

    public static final String FLD = "FLD";


    public static final String ROUTING = "ROUTING";

    public static final String PRD_BASE_CURRENCY = "QAR";


    public static final String REQUEST_ACCEPT_LANG = "Accept-Language";



    public static final String MOREINFORMATION = "moreinformation";


    public static final String MW_BASEURL = "MW_BASEURL";


    public static final String MW_BASEURL_OLD = "MW_BASEURL_OLD";

	public static final String ACT = "ACT";

	public static final String IAC = "IAC";


	public static final String INVALID_MAINTENANCE_CODE = "4700";
	
	public static final String INVALID_MAINTENANCE_DESC = "Invalid maintenance message type";

    public static final String GEN_CHANNEL_EXISTS_CODE = "CH-0001";
    
    public static final String GEN_CHANNEL_EXISTS_DESC = "Channel already exists";
    
    public static final String GEN_CHANNEL_NOT_EXISTS_CODE = "CH-0002";
    
    public static final String GEN_CHANNEL_NOT_EXISTS_DESC = "Channel does not exists";

	public static final String DUPLICATE_LABEL_CODE = "Duplicate Entry in File/DB";

	public static final String DISPLAY_PRIORITY = "disPriority";
	
	public static final String EMPTY_BANNER_LIST_CODE = "4800";
	
	public static final String EMPTY_BANNER_LIST_DESC = "Banner list is empty";
	
	public static final String BANNER_ALREADY_EXISTS_CODE = "4900";
	
	public static final String BANNER_ALREADY_EXISTS_DESC = "Banner already exists";
	
	public static final String BANNER_NOT_EXISTS_CODE = "5000";
	
	public static final String BANNER_NOT_EXISTS_DESC = "Banner does not exists";
	
	public static final String INVALID_ACTION_CODE = "5100";
	
	public static final String INVALID_ACTION_DESC = "Invalid action";
	
	public static final String RESOL_SMALL = "small";
	
	public static final String RESOL_MEDIUM = "medium";
	
	public static final String RESOL_LARGE = "large";
	
	public static final int DISP_PRIORITY_1 = 1;
	
	public static final int DISP_PRIORITY_2 = 2;
	
	public static final int DISP_PRIORITY_3 = 3;

	public static final String UNAUTHORIZED_CODE = "401";
	
	public static final String UNAUTHORIZED_DESC = "UNAUTHORIZED";
	
	public static final String ALREADY_EXISTS_CODE = "EC-00001";
	
	public static final String ALREADY_EXISTS_DESC = "Already Exists";
	
	public static final String NOT_FOUND = "000001";
	
	public static final String LOGIN_CATEGORY_CODE = "LOGIN";
	
	public static final String LOGIN_SUCCESS_CODE = "000000";
	
	public static final String COUNTRY_CODE_NOT_EXISTS= "Given country code dosn't exixts";
	
	public static final String ADD= "ADD";

	public static final String RULE_EXISTS = "Rule already exists";
	
	public static final String RULE_EXISTS_CODE= "E-00002";
	
	public static final String RULE_DOES_NOT_EXISTS = "Rule does not exists";
	
	public static final String RULE_DOES_NOT_EXISTS_CODE= "E-00003";

	public static final String MODIFY= "MOD";
	
	public static final String DELETE= "DEL";

    public static final int ONE_MONTH_IN_DAYS = 30;
    
    public static final String PAYEE_EXISTS = "Payee already exists";
    
    public static final String PAYEE_EXISTS_CODE = "E-0012";
    
    public static final String PAYEE_DOES_NOT_EXISTS = "Payee does not exists";
    
    public static final String PAYEE_DOES_NOT_EXISTS_CODE = "E-00321";
    
    public static final String UTILITY_TYPE_EXISTS = "Utility type already exists";
    
    public static final String UTILITY_TYPE_EXISTS_CODE = "E-00322";
    
    public static final String UTILITY_TYPE_DOES_NOT_EXISTS = "Utility type does not exists";
    
    public static final String UTILITY_TYPE_DOES_NOT_EXISTS_CODE = "E-000973";
    
    public static final String UTILITY_CODE_EXISTS = "Utility code exists";
    
    public static final String UTILITY_CODE_EXISTS_CODE = "E-000900";
    
    public static final String UTILITY_CODE_DOES_NOT_EXISTS = "Utility code does not exists";
    
    public static final String UTILITY_CODE_DOES_NOT_EXISTS_CODE = "E-000903";

    public static final String NO_DATA_FOUND = "No data found";

    public static final String BO_TRANSFER_MONITORING_DAYS = "BO_TRANSFER_MONITORING_DAYS";
	public static final String FCM_KEY = "FCM_KEY";

	public static final String DENOMINATION_EXISTS_CODE = "E-9980";
	
	public static final String DENOMINATION_DOES_NOT_EXISTS_CODE = "E-8880";
}

