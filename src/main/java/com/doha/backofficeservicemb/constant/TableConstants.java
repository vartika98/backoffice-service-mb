package com.doha.backofficeservicemb.constant;

public class TableConstants {

	private TableConstants() {

		throw new IllegalStateException("TableConstants class");

	}

	public static final String TABLE_ERROR_CONF = "OCS_T_ERROR_CONFIG";

	public static final String TABLE_CURRENCY_MASTER = "OCS_T_CURRENCY";

	public static final String TABLE_CURRENCY_DESC = "OCS_T_CURRENCY_DESC";

	public static final String TABLE_PRODUCT_MASTER = "OCS_T_PROD_MASTER";

	public static final String TABLE_SUB_PRODUCT_MASTER = "OCS_T_SUB_PROD_MASTER";
	public static final String TABLE_COUNTRY_MASTER = "OCS_T_COUNTRY";

	// metadata

	public static final String TABLE_CHANNEL_MASTER = "OCS_T_CHANNEL_MASTER";

	public static final String TABLE_CONFIG = "OCS_T_PARAM_CONFIG";

	public static final String TABLE_PAGE_CONFIG = "OCS_T_SCREEN_CONFIG";

	public static final String TABLE_TXN_ENTITLEMENT = "OCS_T_TXN_ENTITLEMENT";

	public static final String TABLE_LANGUAGE_MASTER = "OCS_T_LANGUAGE_MASTER";

	public static final String TABLE_MENU_MASTER = "OCS_T_MENU_MASTER";

	public static final String TABLE_TRANSLATION = "OCS_T_I18N";

	public static final String TABLE_MOB_TRANSLATION = "OCS_T_MOB_I18N";

	public static final String TABLE_UNIT_MASTER = "OCS_T_UNIT_MASTER";

	public static final String OCS_T_UNIT_DESC = "OCS_T_UNIT_DESC";

	public static final String TABLE_UNIT_LANGUAGE = "OCS_T_UNIT_LANGUAGE";

	public static final String TABLE_SCREEN_ENT = "OCS_T_SCREEN_ENTITLEMENT";

	public static final String TABLE_CATEGORY_CODE = "OCS_T_CATEGORY_CODE";

	// user

	public static final String TABLE_USER_SETTINGS = "OCS_T_USER_SETTINGS";

	public static final String TABLE_USER_LAST_LOGIN = "OCS_T_USER_LAST_LOGIN";

	public static final String TABLE_URL_PROVIDER = "OCS_T_URL_PROVIDER";

	public static final String TABLE_RR_MESSAGE = "OCS_T_RR_MESSAGES";

	public static final String TABLE_MB_RR_MESSAGE = "OCS_T_MB_RR_MESSAGES";

	public static final String TABLE_BRANCH_MASTER = "OCS_T_BRANCH_MASTER";

	// App Maintenance

	public static final String TABLE_APP_MAINTENANCE_STAGING = "BKO_T_APPLICATION_REQUEST_STAGING";
	public static final String TABLE_APP_MAINTENANCE_SUB_STAGING = "BKO_T_APPLICATION_SUB_REQUEST_STAGING";

	// Banner

	public static final String TABLE_BANNER = "OCS_T_BANNER";

	public static final String TABLE_OCS_T_CFMS_PARAM = "OCS_T_CFMS_PARAM";

	// Spring Session

	public static final String TABLE_SPRING_SESSION = "SPRING_SESSION";

	public static final String TABLE_SPRING_SESSION_ATTRIBUTES = "SPRING_SESSION_ATTRIBUTES";

	public static final String TABLE_OCS_T_TXN_CATEGORY = "OCS_T_TXN_CATEGORY";
	public static final String TABLE_AUDIT_DETAILS = "OCS_T_AUDIT_DETAILS";
	public static final String TABLE_AUDIT_MASTER = "OCS_T_AUDIT_MASTER";

	public static final String TABLE_BKO_OCS_T_PRODUCT_MASTER = "BKO_OCS_T_PRODUCT_MASTER";

	public static final String TABLE_BKO_OCS_SUB_PRODUCT_MASTER = "BKO_OCS_SUB_PRODUCT_MASTER";

	public static final String TABLE_BKO_OCS_FUNCTION_MASTER = "BKO_OCS_FUNCTION_MASTER";

	public static final String TABLE_BKO_OCS_BO_MENU_ENTITLEMENT = "BKO_OCS_BO_MENU_ENTITLEMENT";

	public static final String TABLE_BKO_OCS_RULES_PARSER = "BKO_OCS_RULES_PARSER";

	public static final String TABLE_BKO_OCS_RULES_MASTER = "BKO_OCS_RULES_MASTER";

	public static final String TABLE_BKO_OCS_RULES_DEFINITION = "BKO_OCS_RULES_DEFINITION";

	public static final String TABLE_BKO_RULES_ACC_MAP_MASTER = "BKO_RULES_ACC_MAP_MASTER";
	public static final String TABLE_BKO_OCS_BO_GROUP_PRO_MAP = "BKO_OCS_T_GROUP_PRODUCT_MAPPING";
	public static final String TABLE_BKO_OCS_GROUPS = "BKO_OCS_T_GROUPS";
	public static final String TABLE_BKO_OCS_BO_GROUP_MASTER = "BKO_OCS_T_GROUP_MASTER";

	public static final String TABLE_TRANSFERS = "OCS_T_TRANSFERS";

	public static final String TABLE_PAYEES = "OCS_T_UTILITY_PAYEES";

	public static final String TABLE_UTILITY_CODES = "OCS_T_UTILITY_CODES";

	public static final String TABLE_UTILITY_TYPES = "OCS_T_UTILITY_TYPES";

	public static final String TABLE_PREPAID_DENOMINATIONS = "OCS_T_UTILITY_PREPAID_DENOMINATIONS";
	public static final String KEY_CONFIG = "OCS_T_KEY_CONFIG";

	public static final String TABLE_BKO_OCS_T_WORKFLOW_HISTORY = "BKO_OCS_T_WORKFLOW_HISTORY";

	public static final String TABLE_OCS_T_BANNER_STAGE = "OCS_T_BANNER_STAGE";
	public static final String TABLE_BKO_OCS_T_DB_SERVICEID = "BKO_OCS_T_DB_SERVICEID";
}
