package com.doha.backofficeservicemb.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.doha.backofficeservicemb.entity.I18N;

@Repository
public interface I18NRepository extends JpaRepository<I18N, Integer> {

	@Query(value = "SELECT * from OCS_T_I18N lbl"
			+ " where lbl.unit_id=:unitId and lbl.channel_id=:channelId AND lbl.SCREEN_ID=:screenId AND lbl.STATUS<>'DEL' ORDER BY date_modified DESC", nativeQuery = true)
	List<I18N> getLabelList(@Param("unitId") String unitId, @Param("channelId") String channelId,
			@Param("screenId") String screenId);

	List<I18N> findByUnit_UnitIdAndChannel_ChannelIdAndPage(String unitId, String channelId, String screenId);

	@Query(value = "SELECT * from OCS_T_I18N lbl"
			+ " where lbl.unit_id=:unitId and lbl.channel_id=:channelId AND lbl.SCREEN_ID=:screenId", nativeQuery = true)
	List<I18N> getAllLabelList(@Param("unitId") String unitId, @Param("channelId") String channelId,
			@Param("screenId") String screenId);

	List<I18N> findByUnit_UnitIdAndChannel_ChannelIdAndLangCodeAndPage(String unitId, String channelId, String langCode,
			String screenId);

	List<I18N> findByUnit_UnitIdAndChannel_ChannelIdAndKeyIn(String unitId, String channelId, List<String> keyLst);

	List<I18N> findByUnit_UnitIdAndChannel_ChannelIdAndKeyInAndStatusNotOrderByCreatedTime(String unitId,
			String channelId, List<String> keyLst, String status);

	List<I18N> findByUnit_UnitIdAndChannel_ChannelIdAndGroupAndKeyInAndStatusNotOrderByCreatedTime(String unitId,
			String channelId, String group, List<String> keyLst, String status);

}
