package com.doha.backofficeservicemb.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;

/*
 * 1.No need to use Lombok
 * 2.only Parameterized constructor get created implicitly 
 */

//@Data
//@NoArgsConstructor
//@AllArgsConstructor
//public class ResultUtilVO implements Serializable {
//
//	private static final long serialVersionUID = 4191307163490295428L;
//
//	private String code;
//
//	private String description;
//
//	@JsonIgnore
//	private String mwCode;
//
//	@JsonIgnore
//	private String mwdesc;
//
//	public ResultUtilVO(String code, String description) {
//		super();
//		this.code = code;
//		this.description = description;
//
//	}
//
//}

public record ResultUtilVO(String code, String description, @JsonIgnore String mwCode, @JsonIgnore String mwdesc)
		implements Serializable {

	private static final long serialVersionUID = 4191307163490295428L;

	public ResultUtilVO() {
		this(null, null, null, null);
	}

	public ResultUtilVO(String code, String description) {
		this(code, description, null, null);
	}

}
