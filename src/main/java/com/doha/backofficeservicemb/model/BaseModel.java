package com.doha.backofficeservicemb.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import jakarta.persistence.Column;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.MappedSuperclass;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicInsert;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import lombok.Data;

@MappedSuperclass
@Data
@DynamicInsert
@EnableJpaAuditing
@EntityListeners(AuditingEntityListener.class)
public class BaseModel implements Serializable {

	private static final long serialVersionUID = 2372526014669295340L;

	@Column(name = "STATUS", insertable = true, updatable = true)
	private String status;

	@CreatedBy
	@Column(name = "CREATED_BY", nullable = false, updatable = false, length = 15)
	@ColumnDefault("SYSTEM")
	private String createdBy;

	@CreatedDate
	@Column(name = "DATE_CREATED", nullable = false, updatable = false)
	private LocalDateTime createdTime;

	@LastModifiedBy
	@Column(name = "MODIFIED_BY", nullable = true, length = 15)
	private String modifiedBy;

	@LastModifiedDate
	@Column(name = "DATE_MODIFIED", nullable = true)
	private LocalDateTime modifiedTime;

}
