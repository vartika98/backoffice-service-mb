package com.doha.backofficeservicemb.entity;

import com.doha.backofficeservicemb.constant.TableConstants;
import com.doha.backofficeservicemb.model.BaseModel;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Table(name = TableConstants.TABLE_CHANNEL_MASTER)
public class Channel extends BaseModel {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_no")
	@SequenceGenerator(name = "seq_no", sequenceName = "OCS_T_CHANNEL_MASTER_SEQ", allocationSize = 1)
	@Column(name = "TXN_ID", nullable = false)
	private Integer txnId;

	@Column(name = "CHANNEL_ID", nullable = false, length = 3, unique = true)
	private String channelId;

	@Column(name = "CHANNEL_DESC", nullable = true, length = 30)
	private String channelDesc;

	@Column(name = "DESCRIPTION", nullable = true, length = 30)
	private String description;

	public Channel(String channel) {
		super();
		this.channelId = channel;
	}

}
