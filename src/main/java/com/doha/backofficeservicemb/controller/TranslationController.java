package com.doha.backofficeservicemb.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.doha.backofficeservicemb.model.GenericResponse;
import com.doha.backofficeservicemb.service.I18Service;



@RestController
@RequestMapping("/txn")
@CrossOrigin(origins = "*", allowedHeaders = "*", maxAge = 360000)
public class TranslationController {
	
	@Autowired
	I18Service i18Service;
	
	
	@PostMapping("/labels/list")
	public GenericResponse<Map<String, String>> getLabelByLang(
			@RequestHeader(name="unit") String unit,
			@RequestHeader(name="channel") String channel,
			@RequestHeader(name="Accept-Language") String lang,
			@RequestHeader(name="screenId") String screenId) {
		return i18Service.getLabelByLang(unit,channel,lang,screenId);
	}

}
